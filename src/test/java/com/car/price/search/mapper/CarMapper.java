package com.car.price.search.mapper;

import com.car.price.search.model.Car;
import com.car.price.search.pages.CarDetailsPage;

import java.util.Arrays;
import java.util.List;

public class CarMapper {

    private static final String SPACE_PATTERN = "\\s*,\\s*";

    public static Car mapToVehicle(final String data) {

        List<String> vehicleData = Arrays.asList(data.split(SPACE_PATTERN));

        Car vehicle = new Car();
        vehicle.setRegistration(vehicleData.get(0));
        vehicle.setMake(vehicleData.get(1));
        vehicle.setModel(vehicleData.get(2));
        vehicle.setColor(vehicleData.get(3));
        vehicle.setYear(vehicleData.get(4));

        return vehicle;
    }

    public static Car mapToActualVehicle(final CarDetailsPage detailsPage) {

        Car actualVehicle = new Car();

        actualVehicle.setRegistration(detailsPage.getRegistration());
        actualVehicle.setMake(detailsPage.getMake());
        actualVehicle.setModel(detailsPage.getModel());
        actualVehicle.setColor(detailsPage.getColor());
        actualVehicle.setYear(detailsPage.getYear());

        return actualVehicle;
    }
}
