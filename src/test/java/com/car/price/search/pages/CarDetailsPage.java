package com.car.price.search.pages;

import com.car.price.search.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CarDetailsPage extends DriverManager {

    @FindBy(xpath = "//dt[text()='Registration']/ancestor::dl/dd")
    private WebElement registration;

    @FindBy(xpath = "//dt[text()='Make']/ancestor::dl/dd")
    private WebElement make;

    @FindBy(xpath = "//dt[text()='Model']/ancestor::dl/dd")
    private WebElement model;

    @FindBy(xpath = "//dt[text()='Colour']/ancestor::dl/dd")
    private WebElement colour;

    @FindBy(xpath = "//dt[text()='Year']/ancestor::dl/dd")
    private WebElement year;


    public String getRegistration() {
        scrollTo(registration);
        return registration.getText();
    }

    public String getMake() {
        scrollTo(make);
        return make.getText();
    }

    public String getModel() {
        scrollTo(model);
        return model.getText();
    }

    public String getColor() {
        scrollTo(colour);
        return colour.getText();
    }

    public String getYear() {
        scrollTo(year);
        return year.getText();
    }
}
