package com.car.price.search.pages;

import com.car.price.search.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckCarPricePage extends DriverManager {

    @FindBy(id = "vrm-input")
    private WebElement registrationNumberField;

    @FindBy(xpath = "//button[text()='Free Car Check']")
    private WebElement freeCarCheckButton;

    public void enterRegistrationNumber(String number) {
        registrationNumberField.clear();
        registrationNumberField.sendKeys(number);
    }

    public String getRegistrationNumber() {
        sleep(1000);
        return registrationNumberField.getAttribute("value");
    }

    public void clickOnFreeCarCheckButton() {
        waitForElementVisibility(freeCarCheckButton, 20, "web element is not visible");
        freeCarCheckButton.click();
    }

}
