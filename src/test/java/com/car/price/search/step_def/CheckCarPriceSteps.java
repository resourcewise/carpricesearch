package com.car.price.search.step_def;

import com.car.price.search.util.FileReader;
import com.car.price.search.driver.DriverManager;
import com.car.price.search.mapper.CarMapper;
import com.car.price.search.model.Car;
import com.car.price.search.pages.CheckCarPricePage;
import com.car.price.search.pages.CarDetailsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.*;

public class CheckCarPriceSteps extends DriverManager {

    private List<String> vehicleRegistrationNumbers;
    private List<Car> actualVehicles;

    CheckCarPricePage checkCarPricePage = new CheckCarPricePage();
    CarDetailsPage carDetailsPage = new CarDetailsPage();

    @Given("^I have car registration details$")
    public void i_have_car_registration_details() throws Throwable {
        vehicleRegistrationNumbers = FileReader.getVehicleRegistrationNumbers();
        assertFalse(vehicleRegistrationNumbers.isEmpty());
    }

    @When("^I check each car registration number$")
    public void i_check_each_car_registration_number() throws Throwable {
        actualVehicles = new ArrayList<>();

        for (String registrationNumber : vehicleRegistrationNumbers) {
            checkCarPricePage.enterRegistrationNumber(registrationNumber);
            checkCarPricePage.clickOnFreeCarCheckButton();

            //Check car registration number
            String actualRegistrationNumber = checkCarPricePage.getRegistrationNumber().replaceAll("\\s", "");
            String expectedRegistrationNumber = registrationNumber.replaceAll("\\s", "");
            assertThat(actualRegistrationNumber, is(equalTo(expectedRegistrationNumber)));

            //Verify car details with expected values from car_output.txt
            Car actualVehicle = CarMapper.mapToActualVehicle(carDetailsPage);
            actualVehicles.add(actualVehicle);
            gotoHomePage();
        }

    }

    @Then("^I can verify car details with expected details$")
    public void i_can_verify_car_details_with_expected_details() throws Throwable {
        Map<String, Car> expectedVehicles = FileReader.getExpectedVehicleData();

        //car details with expected values from car_output.txt
//        actualVehicles.forEach(actualVehicle -> {
//            Car expectedVehicle = expectedVehicles.get(actualVehicle.getRegistration());
//            assertThat(actualVehicle, samePropertyValuesAs(expectedVehicle));
//        });
    }
}
